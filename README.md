This is an experimental Void Linux version of the Asahi installer for Apple Macs that use the M series ARM64 processor, also known as Apple Silicon.

See https://asahilinux.org/ for more information about the Asahi Linux project.

__***To use this installer you should be famiiar with the void linux install instructions and should be able to configure a Linux system from scratch***__

Quick start; from a macOS terminal run:

```curl -L https://tinyurl.com/void-asahi | sh```

Make sure to follow the instructions carefully!

Once installed, you will be able to boot into a basic live system (login `root` with password `voidlinux`) but from there you are on your own. 

_Tip: although this is a live system it is installed to disk so you should probably treat it as a chroot and follow the rootfs method instead._

Audio and GPU support may be built with xbps-src using the asahi2 branch of [dkwo's void-packages repo](https://github.com/dkwo/void-packages/tree/asahi2). 
See also [https://github.com/dkwo/void-packages/blob/asahi2/srcpkgs/asahi-base/files/README.voidlinux](https://github.com/dkwo/void-packages/blob/asahi2/srcpkgs/asahi-base/files/README.voidlinux) for Void specific aspects.


## License

Copyright The Asahi Linux Contributors

The Asahi Linux installer is distributed under the MIT license. See LICENSE for the license text.

This installer vendors [python-asn1](https://github.com/andrivet/python-asn1), which is distributed under the same license.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
